# Sistema de Tickets Servicio Técnico

Se propone desarrollar un sistema, que administre el los tickets de servicio técnico, de computadoras, de un local comercial.
Los clientes  llevan al local sus computadoras (Notebooks, Netbooks, PC Escritorio), para reparar. En el local, se registra el cliente, se le asigna un nro. de ticket, en el cual se detallan los problemas del equipo, un tiempo estimado de diagnostico/reparación, y un empleado para llevar a cabo la reparación.
Al ingresar el equipo se le coloca el estado de Pendiente. Cuando el empleado comienza la revisión pasa al estado de "En Reparación".
Cuando el equipo se repara, o no tiene solución, se cambia el estado de ticket a: "Lista Reparada" o "Lista No reparada" más una descripción de las tareas realizadas, y se avisa al cliente para que retire su computadora.
Si el cliente retira su equipo, el ticket se cierra.
Estados de ticket: Pendiente, En Reparacion, Lista Reparada, Lista No reparada, Cerrado.

Restricciones:
- Un cliente puede tener más de una computadora.
- Una computadora pertenece solo a un cliente.
- No puede existir dos clientes con el mismo documento.
- No puede existir dos empleados con el mismo legajo.

Funcionalidad
- Gestionar Clientes (Crear y Modificar)
- Gestionar Tipos de Equipos (Crear y Modificar)
- Gestionar Tickets (Crear y Modificar)
- Gestionar Empleados (Crear y Modificar)
- Gestionar Equipos (Crear y Modificar)
- Buscar Clientes (todos, por identificador y por criterio a definir)
- Buscar Tickets (todos, por identificador y por criterio a definir)
- Buscar Empleados (todos, por identificador y por criterio a definir


[MODELO DER](https://gitlab.com/d7096/proyecto-integrador/-/blob/main/DER.png)
